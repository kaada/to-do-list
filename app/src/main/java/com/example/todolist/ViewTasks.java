package com.example.todolist;

import android.content.Context;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public class ViewTasks extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_tasks);

        //reads task file
        String ret = "";
        try {
            InputStream inputStream = openFileInput(MainActivity.fileName);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);


                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();

            }
        } catch(FileNotFoundException fnfe){
            System.err.println("Error: Can not find file while reading from file in ViewTasks");
        } catch (IOException ioe){
            System.err.println("Error: Gets IOException while reading from file in ViewTasks");
        }


        String[] dateStringArr = ret.split("/");
        int day = Integer.parseInt(dateStringArr[0]);
        int month = Integer.parseInt(dateStringArr[1]);
        int year = Integer.parseInt(dateStringArr[2]);
        String taskText = dateStringArr[3];
    }
}
