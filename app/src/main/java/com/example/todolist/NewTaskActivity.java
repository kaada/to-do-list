package com.example.todolist;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.util.Calendar;

import static com.example.todolist.MainActivity.fileName;

public class NewTaskActivity extends AppCompatActivity {

    private TextView mDisplayDate;
    private DatePickerDialog.OnDateSetListener mDataSetListener;
    private boolean dateSelected = false;
    private int day,month,year;
    private byte[] byteStream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);
        mDisplayDate = (TextView) findViewById(R.id.tvDate);

        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        NewTaskActivity.this,
                        android.R.style.Theme_DeviceDefault_Dialog_NoActionBar_MinWidth,
                        mDataSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
                dateSelected = true;
            }
        });

        mDataSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year1, int month1, int dayOfMonth) {
                month += 1;

                String date = dayOfMonth + "/" + month1 + "/" + year1;
                System.out.println("HOY1: " + date);
                day=dayOfMonth;
                month=month1;
                year=year1;

                mDisplayDate.setText(date);
            }
        };



        Button addTaskButton = (Button) findViewById(R.id.addTask);
        addTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!dateSelected){
                    new AlertDialog.Builder(NewTaskActivity.this)
                            .setTitle("You must select date!")
                            .setPositiveButton(android.R.string.ok, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                } else{
                    EditText editText = (EditText) findViewById(R.id.taskText);
                    String text = editText.getText().toString();
                    System.out.println("TEXT: " + text + "\nDATE: " + year + "-"+month+"-"+day);
                    String stringToStore = day + "/" + month + "/" + year + "/" + text;
                    byte[] byteStream = stringToStore.getBytes();

                    //print to file
                    try{
                        FileOutputStream outputStream = getApplicationContext().openFileOutput(fileName, Context.MODE_PRIVATE);
                        outputStream.write(byteStream);
                        outputStream.close();
                    } catch(FileNotFoundException fnfe){
                        System.err.println("Error: Can not find file while printing to file in NewTaskActivity");
                    } catch(IOException ioe){
                        System.err.println("Error: IO Exception while printing to file in NewTaskActivity");
                    }


                }

            }
        });

    }
}
